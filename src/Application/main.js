import Vue from 'vue';
import Page from './Components/Page.vue';

new Vue({
    el: '.app',
    render: h => h(Page)
});


// import Vue from 'vue';
// import Vuex from 'vuex';
//
// import Page from './Components/Page.vue';
// import RootStore from './Store/RootStore';
//
// Vue.use(Vuex)
// //Vue.use(require('vue-resource'));
//
// new Vue({
//     el: '.app',
//     store: new Vuex.Store(RootStore()),
//     render: h => h(Page)
// });