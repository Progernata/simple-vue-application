/**
 Примеры ссылок ресурсов API:
 Объявления о продаже авто http://auto.ngs.ru/api/v1/offers/
 Вакансии работодателей http://rabota.ngs.ru/api/v1/vacancies/
 Поиск недвижимости в Новосибирске http://api.realty.ngs.ru/api/v1/offers/all/?region=54
 Текущий прогноз погоды в Новосибирске http://pogoda.ngs.ru/api/v1/forecasts/current?city=novosibirsk
 **/

function getVacancies(params) {
    params.fields = ['header', 'id'];
    let esc = encodeURIComponent;
    let query = Object.keys(params)
        .map(k => esc(k) + '=' + esc(params[k]))
        .join('&');

    return fetch('http://rabota.ngs.ru/api/v1/vacancies/?' + query)
        .then((response) => {
            return response.json();
        })
}

const mutations = {
    increment: '_increment'
}

const moduleA = {
    state: {count: 0},
    mutations: {
        [mutations.increment]: state => state.count++
    },
    actions: {},
    getters: {
        solo: (state) => {
            return state.count;
        }
    }
}


export default function () {

    return {
        state: {
            count: 0,
            vacancies: []
        },
        mutations: {
            vacancies: (state, vacancies) => state.vacancies = vacancies,
            increment: state => state.count++,
            decrement: state => state.count--
        },
        getters: {
            blah: (state) => {
                return state.count;
            }
        },
        actions: {
            async increment ({commit, state}) {
                try {
                    commit('increment');

                    const {vacancies} = await getVacancies({
                        limit: 5,
                        offset: state.count
                    });

                    commit('vacancies', vacancies);

                } catch (e) {
                    console.error(e);
                }
            }
        },
        modules: {
            moduleA
        }
    }
}
